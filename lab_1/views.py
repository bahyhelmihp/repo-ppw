from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Bahy Helmi H P' # TODO Implement this
birth_year = 1997

# Calculate age
def calculate_age(birth_year):
    today = date.today()
    return today.year - birth_year

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_year)}
    return render(request, 'index.html', response)